FROM openjdk:17
EXPOSE 8080
ADD target/*.war java-app.war
ENTRYPOINT ["java","-jar", "/java-app.war"]